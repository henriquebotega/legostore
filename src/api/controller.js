var properties = require("../../package.json");
var distance = require("../services/distance");

var controllers = {
	about: (req, res) => {
		var info = {
			name: properties.name,
			version: properties.version,
		};

		res.json(info);
	},
	getDistance: (req, res) => {
		distance.find(req, res, (err, dist) => {
			if (err) {
				res.send(err);
			}

			res.json(dist);
		});
	},
};

module.exports = controllers;
