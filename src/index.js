const express = require("express");
const port = process.env.PORT || 3000;

const routes = require("./api/routes");

const app = express();
routes(app);

app.listen(port, () => {
	console.log("Running on port " + port);
});
