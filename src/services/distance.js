var request = require("request");

const apiKey = process.env.ZIPCODE_API_KEY || "YQbguoUj3Jt5lPYEKYZq5KzmaaIuflRmIHYKtv7CHXuBGvIvysGwnQd2kG0qJM4P";
const zipCodeURL = "https://www.zipcodeapi.com/rest/";

var distance = {
	find: function (req, res, next) {
		var url = zipCodeURL + apiKey + "/distance.json/" + req.params.zipcode1 + "/" + req.params.zipcode2 + "/mile";
		request(url, (error, response, body) => {
			if (!error && response.statusCode == 200) {
				response = JSON.parse(body);
				res.send(response);
			} else {
				console.log(response.statusCode + " = " + response.body);
				res.send({ distance: -1 });
			}
		});
	},
};

module.exports = distance;
